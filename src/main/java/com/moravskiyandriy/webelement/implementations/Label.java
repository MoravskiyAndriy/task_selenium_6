package com.moravskiyandriy.webelement.implementations;

import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.BasicWebElement;
import org.openqa.selenium.WebElement;

public class Label extends BasicWebElement {
    public Label(WebElement WEBElement) {
        super(WEBElement);
    }

    public String getText(){
        Waiter.waitForVisibilityOfElement(webElement, Waiter.MEDIUM_WAIT);
        return webElement.getText();
    }

    public boolean isDisplayed(){
        return webElement.isDisplayed();
    }
}
