package com.moravskiyandriy.webelement.implementations;

import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.BasicWebElement;
import org.openqa.selenium.WebElement;

public class InputTextField extends BasicWebElement {
    public InputTextField(WebElement WEBElement) {
        super(WEBElement);
    }

    public void sendKeys(CharSequence... charSequences) {
        Waiter.waitForVisibilityOfElement(webElement, Waiter.LONG_WAIT);
        webElement.clear();
        webElement.sendKeys(charSequences);
    }

    public String getText() {
        Waiter.waitForVisibilityOfElement(webElement, Waiter.SMALL_WAIT);
        return webElement.getText();
    }
}
