package com.moravskiyandriy.webelement.implementations;

import com.moravskiyandriy.utils.DriverManager;
import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.BasicWebElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Button extends BasicWebElement {
    public Button(WebElement WEBElement) {
        super(WEBElement);
    }

    public void click() {
        Waiter.waitForElementToBeClickable(webElement,Waiter.LONG_WAIT);
        webElement.click();
    }

    public void doubleClick() {
        Waiter.waitForVisibilityOfElement(webElement, Waiter.SMALL_WAIT);
        new Actions(DriverManager.getDriver()).doubleClick(webElement);
    }
}
