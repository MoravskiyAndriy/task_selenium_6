package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.implementations.Button;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class ReverseActionPopUp extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(ReverseActionPopUp.class);
    @FindBy(css = "span#link_undo")
    private Button undoButton;

    public void clickUndoButton() {
        Waiter.waitForVisibilityOfElement(undoButton, Waiter.MEDIUM_WAIT);
        LOGGER.info("clicking Undo Button");
        undoButton.click();
    }
}
