package com.moravskiyandriy.pageobjects;

import com.moravskiyandriy.utils.DriverManager;
import com.moravskiyandriy.decorator.CustomFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class AbstractPageObject {
    protected WebDriver driver;

    protected AbstractPageObject() {
        driver = DriverManager.getDriver();
        PageFactory.initElements(new CustomFieldDecorator(
                new DefaultElementLocatorFactory(DriverManager.getDriver())), this);
    }
}
